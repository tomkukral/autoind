module gitlab.com/tomkukral/autoind

go 1.12

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/olekukonko/tablewriter v0.0.1
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
	golang.org/x/text v0.3.0
	gopkg.in/yaml.v2 v2.2.2
)
