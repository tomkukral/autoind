package command

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/tomkukral/autoind/pkg/car"
	"gitlab.com/tomkukral/autoind/pkg/db"
	"gitlab.com/tomkukral/autoind/pkg/fetch"
)

func init() {
	rootCmd.AddCommand(fetchCmd)
}

var fetchCmd = &cobra.Command{
	Use:   "fetch",
	Short: "Load new cars to database",
	Run: func(cmd *cobra.Command, args []string) {
		file := cmd.Flag("file").Value.String()

		carDB := car.DB{}

		// load cars
		if err := db.Load(file, carDB); err != nil {
			log.Fatal(err)
		}

		if err := fetch.Run(carDB); err != nil {
			log.Fatal(err)
		}

		// save cars
		if err := db.Save(file, carDB); err != nil {
			log.Fatal(err)
		}

	},
}
