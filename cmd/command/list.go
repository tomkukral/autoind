package command

import (
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/tomkukral/autoind/pkg/car"
	"gitlab.com/tomkukral/autoind/pkg/db"
)

func init() {
	rootCmd.AddCommand(listCmd)
}

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "Print list of cars",
	Run: func(cmd *cobra.Command, args []string) {
		file := cmd.Flag("file").Value.String()

		carDB := car.DB{}

		// load cars
		if err := db.Load(file, carDB); err != nil {
			log.Fatal(err)
		}

		lines := [][]string{}

		// print table
		table := tablewriter.NewWriter(os.Stdout)
		table.SetAutoWrapText(false)
		table.SetHeader([]string{
			"Score",
			"Name",
			"Price",
			"Tacho",
			"Locality",
			"Year",
			"Link",
		})

		// calculate score
		for _, c := range carDB {
			score := c.Score()
			if score <= 0 {
				continue
			}

			lines = append(lines, []string{
				fmt.Sprintf("%d", score),
				c.AdvertName,
				fmt.Sprintf("%d", c.AdvertTachometr/1000),
				fmt.Sprintf("%d", c.AdvertPriceTotal/1000),
				c.AdvertLocalityDistrict,
				fmt.Sprintf("%d", c.AdvertMadeDate),
				c.AdvertLocalityDistrict,
				c.URL(),
			})
		}

		// sort lines by score
		sort.SliceStable(lines, func(i, j int) bool {
			si, err := strconv.ParseInt(lines[i][0], 10, 32)
			if err != nil {
				return false
			}

			sj, err := strconv.ParseInt(lines[j][0], 10, 32)
			if err != nil {
				return false
			}

			return sj < si
		})

		table.AppendBulk(lines)
		table.Render()

	},
}
