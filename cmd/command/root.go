package command

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.PersistentFlags().String("file", "cars.yml", "Filename to store car database")
}

var rootCmd = &cobra.Command{
	Use:   "carind",
	Short: "Car indexer and qualifier",
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
