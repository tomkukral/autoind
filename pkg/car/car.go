package car

import (
	"log"
	"time"
)

type DB map[int]*Car

// AddCars add list of cars to existing car database
func AddToDB(db DB, cars []*Car) {
	for _, c := range cars {
		cd, ok := db[c.AdvertID]
		if !ok {
			// new car
			c.FirstSeen = time.Now()
			c.LastSeen = time.Now()
			db[c.AdvertID] = c

			log.Printf("Added new car %s", c.String())
		} else {
			// existing car
			cd.LastSeen = time.Now()
			db[c.AdvertID] = c

			log.Printf("Updated car %s", c.String())
		}

	}
}
