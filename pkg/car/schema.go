package car

import (
	"fmt"
	"time"
)

type Car struct {
	CarMeta
	CarInfo
}

func (c *Car) String() string {
	return fmt.Sprintf("%s %s - %s (%d)", c.ManufacturerName, c.ModelName, c.AdvertName, c.AdvertID)
}

// Score calculates score for car
func (c *Car) Score() int {
	s := 0

	// first owner
	if c.AdvertFirstOwner == 1 {
		s += 10
	}

	// too expensive
	if c.AdvertPriceTotal > 300000 {
		s -= 30
	}

	// too old
	if c.AdvertMadeDate <= 2003 {
		s -= 10
	}

	return s
}

func (c *Car) URL() string {
	return fmt.Sprintf("https://www.sauto.cz/osobni/detail/volkswagen/transporter/%d", c.AdvertID)
}

type CarMeta struct {
	FirstSeen time.Time
	LastSeen  time.Time
}

type CarInfo struct {
	CategoryMainCb         int           `json:"category_main_cb"`
	AdvertPriceLeasing     int           `json:"advert_price_leasing"`
	AdvertCrPdf            string        `json:"advert_cr_pdf"`
	PremiseShowLogo        int           `json:"premise_show_logo"`
	AdvertLocalityDistrict string        `json:"advert_locality_district"`
	PremiseName            string        `json:"premise_name"`
	ManufacturerID         int           `json:"manufacturer_id"`
	AdvertConditionCb      string        `json:"advert_condition_cb"`
	AdvertPerex            string        `json:"advert_perex"`
	PremiseLogo            string        `json:"premise_logo"`
	AdvertTachometr        int           `json:"advert_tachometr"`
	AdvertTachometrUnitCb  string        `json:"advert_tachometr_unit_cb"`
	AdvertRunDate          int           `json:"advert_run_date"`
	AdvertColorCb          string        `json:"advert_color_cb"`
	AdvertCertification    int           `json:"advert_certification"`
	AdvertEngineVolume     int           `json:"advert_engine_volume"`
	AdvertCrRating         int           `json:"advert_cr_rating"`
	AdvertPayment          int           `json:"advert_payment"`
	AdvertFirstOwner       int           `json:"advert_first_owner"`
	AdvertID               int           `json:"advert_id"`
	AdvertLocalityCity     string        `json:"advert_locality_city"`
	AdvertURL              string        `json:"advert_url"`
	History                int           `json:"history"`
	Compare                int           `json:"compare"`
	AdvertPaymentCount     int           `json:"advert_payment_count"`
	AdvertForHandicapped   int           `json:"advert_for_handicapped"`
	AdvertSince            string        `json:"advert_since"`
	AdvertName             string        `json:"advert_name"`
	AdvertLabels           []interface{} `json:"advert_labels"`
	ManufacturerName       string        `json:"manufacturer_name"`
	AdvertImage            string        `json:"advert_image"`
	AdvertEquipmentSet7    string        `json:"advert_equipment_set7"`
	AdvertEquipmentSet6    string        `json:"advert_equipment_set6"`
	AdvertEquipmentSet5    string        `json:"advert_equipment_set5"`
	AdvertEquipmentSet4    string        `json:"advert_equipment_set4"`
	AdvertEquipmentSet3    string        `json:"advert_equipment_set3"`
	AdvertEquipmentSet2    string        `json:"advert_equipment_set2"`
	AdvertEquipmentSet1    string        `json:"advert_equipment_set1"`
	AdvertEquipmentSet0    string        `json:"advert_equipment_set0"`
	AdvertServiceBook      int           `json:"advert_service_book"`
	AdvertMadeDate         int           `json:"advert_made_date"`
	AdvertAirconditionCb   int           `json:"advert_aircondition_cb"`
	AdvertPriceTotal       int           `json:"advert_price_total"`
	AdvertEdited           string        `json:"advert_edited"`
	AdvertFuelCb           string        `json:"advert_fuel_cb"`
	AdvertCrJpg            string        `json:"advert_cr_jpg"`
	AdvertGearboxCb        int           `json:"advert_gearbox_cb"`
	AdvertPriceSale        int           `json:"advert_price_sale"`
	AdvertImageObjectID    string        `json:"advert_image_object_id"`
	AdvertAirbagCb         int           `json:"advert_airbag_cb"`
	ModelName              string        `json:"model_name"`
}
