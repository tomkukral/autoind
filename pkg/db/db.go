package db

import (
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/tomkukral/autoind/pkg/car"
	yaml "gopkg.in/yaml.v2"
)

func Save(file string, db car.DB) error {
	// marshal
	sc, err := yaml.Marshal(db)
	if err != nil {
		return err
	}

	// write file
	return ioutil.WriteFile(file, sc, 0644)
}

func Load(file string, db car.DB) error {
	dat, err := ioutil.ReadFile(file)
	if err != nil {
		if os.IsNotExist(err) {
			log.Printf("File %s doesn't exist, cars won't be loaded", file)
			return nil
		}

		return err
	}

	// unmarshal
	return yaml.Unmarshal(dat, &db)
}
