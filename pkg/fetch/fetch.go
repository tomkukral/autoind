package fetch

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/tomkukral/autoind/pkg/car"
	"gitlab.com/tomkukral/autoind/pkg/utils"
)

type Page struct {
	Cars   []*car.Car `json:"advert"`
	Paging struct {
		Page       int `json:"page"`
		TotalPages int `json:"totalPages"`
	} `json:"paging"`
}

func Run(db car.DB) error {
	// get first page
	p, err := getPage(1, db)
	if err != nil {
		return err
	}

	log.Printf("Number of pages: %d", p.Paging.TotalPages)

	// loop all pages, run goroutines? :)
	for i := 2; i <= p.Paging.TotalPages; i++ {
		_, err := getPage(i, db)
		if err != nil {
			return err
		}

		log.Printf("Got %d cars", len(db))
	}

	// print car info
	template := `{{ .ManufacturerName }} {{ .ModelName }} - {{ .AdvertName }}
  Price: {{ .AdvertPriceTotal }}
  MadeDate: {{ .AdvertMadeDate }}
  Tachometr: {{ .AdvertTachometr }} {{ .AdvertTachometrUnitCb }}
  Color: {{ .AdvertColorCb }}
  First owner: {{ .AdvertFirstOwner }}
  Loc: {{ .AdvertLocalityDistrict }} - {{ .AdvertLocalityCity }}	
  Premise: {{ .PremiseName }}
  Condition: {{ .AdvertConditionCb }}
  History: {{ .History }}
  Labels: {{ .AdvertLabels }}
  Since: {{ .AdvertSince }}
  URL: https://www.sauto.cz/osobni/detail/volkswagen/transporter/{{ .AdvertID }}
	`
	for _, c := range db {
		s, err := utils.Render(template, c)
		if err != nil {
			fmt.Printf("%#v\n", c)
			return err
		}
		fmt.Println(s)
	}

	return nil

}

func getPage(num int, db car.DB) (Page, error) {
	var p Page

	log.Printf("Loading page %d", num)

	// load cars
	listURL := fmt.Sprintf("https://www.sauto.cz/hledani?ajax=2&page=%d&condition=4&condition=2&condition=1&category=4&manufacturer=104&model=774&nocache=665", num)

	resp, err := http.Get(listURL)
	if err != nil {
		return p, fmt.Errorf("Failed to get list page: %s", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return p, fmt.Errorf("Failed reading body: %s", err)
	}

	// parse
	if err := json.Unmarshal(body, &p); err != nil {
		log.Printf("Got body: %s", string(body))
		return p, fmt.Errorf("Failed parsing: %s", err)
	}

	// add cars
	car.AddToDB(db, p.Cars)

	return p, nil
}
