package utils

import (
	"bytes"
	"fmt"
	"text/template"
)

func Render(t string, context interface{}) (string, error) {
	var rb bytes.Buffer

	tpl, err := template.New("tpl").Parse(t)
	if err != nil {
		return "", fmt.Errorf("Failed to parse template: %s", err)
	}
	err = tpl.Execute(&rb, context)
	if err != nil {
		return "", fmt.Errorf("Failed to execute template: %s", err)
	}

	return rb.String(), nil

}
